import React, { Component } from 'react';
import { connect } from 'react-redux';
import {postPerson} from '../../actions'
class PersonForm extends Component {
  
  handleSubmit = (e) => {
    e.preventDefault();
    e.preventDefault();
    const name = this.getName.value;
    const data = {
      name,
      editing: false
    }
   this.props.postPerson(data)

    this.getName.value = '';
  }
  render() {
    return (
      <div>
        <div className="card-body">
          <form role="form" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="title">Name</label>
                <input id="title" required type="text" className="form-control" ref={(input) => this.getName = input} placeholder="Enter Name" />
              </div>
            {/* /.card-body */}
            <div className="card-footer">
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}


export default connect(null,{postPerson})(PersonForm);
