import React, { Component } from 'react';
import { connect } from 'react-redux';


class EditComponent extends Component {
handleEdit = (e) => {
  e.preventDefault();
  const newName = this.getName.value;
  const newSurname = this.getSurname.value;
  const data = {
    newName,
    newSurname
  }
  //this.props.dispatch({ type: 'UPDATE', id: this.props.person.id, data: data })
}
render() {
return (
<div>
  <form onSubmit={this.handleEdit}>
      <div className="form-group">
      <input className="form-control" required type="text" ref={(input) => this.getName = input}
    defaultValue={this.props.person.name} placeholder="Enter Person Name" /> 
      </div>
      <div className="form-group">
      <input className="form-control" required type="text" ref={(input) => this.getSurname = input}
    defaultValue={this.props.person.surname} placeholder="Enter Person Surname" /> 
      </div>
    <button className="btn btn-primary">Update</button>
  </form>
</div>
);
}
}
export default connect()(EditComponent);
