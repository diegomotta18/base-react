import React, { Component } from 'react';
import { connect } from 'react-redux';

import PersonForm from './PersonForm';
import Person from './Person';
import {loadPersons} from '../../actions'
import EditComponent from './EditComponent';

class AllPerson extends Component {
    componentWillMount() {
        this.props.loadPersons()
    }
  
  render() {

    return (
                <div>
                    <div className="content-wrapper">
                        {/* Content Header (Page header) */}
                        <div className="content-header">
                            <div className="container-fluid">
                                <div className="row mb-2">
                                    <div className="col-sm-6">
                                        <h1 className="m-0 text-dark">Persons</h1>
                                    </div>{/* /.col */}
                                    <div className="col-sm-6">
                                        <ol className="breadcrumb float-sm-right">
                                            <li className="breadcrumb-item"><a href="#home">Home</a></li>
                                            <li className="breadcrumb-item active">Persons</li>
                                        </ol>
                                    </div>{/* /.col */}
                                </div>{/* /.row */}
                            </div>{/* /.container-fluid */}
                        </div>
                        {/* /.content-header */}
                        {/* Main content */}
                        <section className="content">
                            <div className="container-fluid">
    
                                {/* Main row */}
                                <div className="row">
                                    {/* Left col */}
                                    <section className="col-lg-12 ">
                                        {/* Custom tabs (Charts with tabs)*/}
    
                                        <div className="card card-primary">
                                            <div className="card-header">
                                                <h3 className="card-title">Quick Example</h3>
                                            </div>
                                            {/* /.card-header */}
                                              <PersonForm />
          
                                            {/* /.card-body */}
                                            <div className="card-footer">
                                                {
                                                this.props.persons.map((person) => (
                                                    <div key={person.id}>
                                                            <Person key={person.id} person={person} />
                                                            
                                                    </div>
                                                ))
                                          
                                            }
                                            </div>
                                        </div>
                                    
                                    </section>
    
                                </div>
                            </div>
                        </section>
                    </div>
                </div> 
    );
   }
}


function mapStateToProps(state) {
    return {
      persons: state.personReducer.remotePersons
    };
  }
export default connect(mapStateToProps,{ loadPersons })(AllPerson);
