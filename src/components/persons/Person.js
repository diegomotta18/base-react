import React, { Component } from 'react';

import {connect} from 'react-redux';

class Person extends Component {
  render() {
  return (
    <div>
      
      <h2>{this.props.person.name}</h2>
      <button
       className="btn btn-success"
       onClick={()=>this.props.dispatch({type:'EDIT_PERSON',id:this.props.person.id})}>
       Edit</button>
      <button 
      className="btn btn-danger"
      onClick={()=>this.props.dispatch({type:'DELETE_PERSON',id:this.props.person.id})}>
      Delete</button>
    </div>
  );
 }
}
export default connect()(Person);
