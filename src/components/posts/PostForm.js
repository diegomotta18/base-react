import React, { Component } from 'react';
import { connect } from 'react-redux';
class PostForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    const title = this.getTitle.value;
    const message = this.getMessage.value;
    const data = {
      id: new Date(),
      title,
      message,
      editing: false
    }
    this.props.dispatch({
      type: 'ADD_POST',
      data
    });

    this.getTitle.value = '';
    this.getMessage.value = '';
  }
  render() {
    return (
      <div>
        <div className="card-body">
          <form role="form" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="title">Titulo</label>
                <input id="title" required type="text" className="form-control" ref={(input) => this.getTitle = input} placeholder="Enter Post Title" />
              </div>
              <div class="form-group">
                <label>Textarea</label>
                <textarea class="form-control" required rows="5" ref={(input) => this.getMessage = input}></textarea>
              </div>
            {/* /.card-body */}
            <div className="card-footer">
              <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    person: state.personReducer.remotePersons
  };
}

export default connect()(PostForm);
