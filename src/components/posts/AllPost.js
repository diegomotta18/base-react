import React, { Component } from 'react';
import { connect } from 'react-redux';

import PostForm from './PostForm';
import Post from './Post';

import EditComponent from './EditComponent';
class AllPost extends Component {

  render() {

    return (
                <div>
                    <div className="content-wrapper">
                        {/* Content Header (Page header) */}
                        <div className="content-header">
                            <div className="container-fluid">
                                <div className="row mb-2">
                                    <div className="col-sm-6">
                                        <h1 className="m-0 text-dark">Posts</h1>
                                    </div>{/* /.col */}
                                    <div className="col-sm-6">
                                        <ol className="breadcrumb float-sm-right">
                                            <li className="breadcrumb-item"><a href="#home">Home</a></li>
                                            <li className="breadcrumb-item active">Posts</li>
                                        </ol>
                                    </div>{/* /.col */}
                                </div>{/* /.row */}
                            </div>{/* /.container-fluid */}
                        </div>
                        {/* /.content-header */}
                        {/* Main content */}
                        <section className="content">
                            <div className="container-fluid">
    
                                {/* Main row */}
                                <div className="row">
                                    {/* Left col */}
                                    <section className="col-lg-12 ">
                                        {/* Custom tabs (Charts with tabs)*/}
    
                                        <div class="card card-primary">
                                            <div class="card-header">
                                                <h3 class="card-title">Quick Example</h3>
                                            </div>
                                            {/* /.card-header */}
                                              <PostForm />
          
                                            {/* /.card-body */}
                                            <div className="card-footer">
                                            {this.props.postReducer.map((post) => (
                                                <div key={post.id}>
                                                    {post.editing ? <EditComponent post={post} key={post.id} /> :
                                                        <Post key={post.id} post={post} />
                                                        }
                                                </div>
                                            ))}
                                            </div>
                                        </div>
                                    
                                    </section>
    
                                </div>
                            </div>
                        </section>
                    </div>
                </div> 
    );
   }
}
const mapStateToProps = state => ({
    ...state
  })

export default connect(mapStateToProps)(AllPost);
