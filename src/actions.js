import axios from 'axios';

export const LOAD_PERSONS_LOADING = 'LOAD_PERSONS_LOADING';
export const LOAD_PERSONS_SUCCESS = 'LOAD_PERSONS_SUCCESS';
export const LOAD_PERSONS_ERROR = 'LOAD_PERSONS_ERROR';
export const CREATE_PERSON = 'CREATE_PERSON';
export const CREATE_ERROR = 'CREATE_ERROR';

 
export const loadPersons = () => dispatch => {
   dispatch({ type: LOAD_PERSONS_LOADING });

   axios.get('http://localhost:3001/actors.json')
       .then(
        data => dispatch({ type: LOAD_PERSONS_SUCCESS, payload: data.data }),
           error => dispatch({ type: LOAD_PERSONS_ERROR, error: error.message || 'Unexpected Error!!!' })
       )
};

export const postPerson = (request) => dispatch => { 
    axios.post('http://localhost:3001/actors.json',request)
        .then(
         data => dispatch({ type: CREATE_PERSON, payload: data }),
        error => dispatch({ type: CREATE_ERROR, error: error.message || 'Unexpected Error!!!' })
        )
 };
