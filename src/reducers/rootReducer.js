import { combineReducers } from 'redux';
import postReducer from './postReducer';
import personReducer from './personReducer';

export default combineReducers({
    postReducer,personReducer
});