import {LOAD_PERSONS_ERROR, LOAD_PERSONS_LOADING, LOAD_PERSONS_SUCCESS, CREATE_PERSON} from "../actions";
const initialState = {
  persons: [],
  remotePersons: [],
  person: null
}
const personReducer = (state = initialState, action) => {
  switch(action.type) {
  
      case LOAD_PERSONS_LOADING: {
          return {
              ...state,
              loading: true,
              error:''
          };
      }
      case LOAD_PERSONS_SUCCESS: {
        let a = Object.assign({}, state, {
          remotePersons: state.remotePersons.concat(action.payload.data)
        });
        return a;
      }
      case LOAD_PERSONS_ERROR: {
          return {
              ...state,
              loading: false,
              error: action.error
          };
      }
      case CREATE_PERSON:
        return { ...state, person: action.payload.data};

      default:
        return state;
    }
  }
  export default personReducer;
  


