import React, { Component } from 'react'
import Dashboard from './Dashboard';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";
import AllPost from './components/posts/AllPost';
import AllPerson from './components/persons/AllPerson';


export default class MenuMain extends Component {

    render() {
        return (
            <Router>

                    <div>
                        <aside className="main-sidebar sidebar-dark-primary elevation-4">
                            {/* Brand Logo */}
                            <a href="index3.html" className="brand-link">
                                <img src="dist/img/AdminLTELogo.png" alt="AdminLTE" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
                                <span className="brand-text font-weight-light">AdminLTE 3</span>
                            </a>
                            {/* Sidebar */}
                            <div className="sidebar">
                                {/* Sidebar user panel (optional) */}
                                <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                                    <div className="image">
                                        <img src="dist/img/user2-160x160.jpg" className="img-circle elevation-2" alt="User" />
                                    </div>
                                    <div className="info">
                                        <a href="fake_url" className="d-block">Alexander Pierce</a>
                                    </div>
                                </div>
                                {/* Sidebar Menu */}
                                <nav className="mt-2">

                                    <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                        {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}
                                <li className="nav-item has-treeview menu-open">
                                    <Link  className="nav-link" to="dashboard">
                                        <i className="nav-icon fas fa-tachometer-alt" /><p>Dashboard</p>
                                    </Link>
                                </li>        
                                <li className="nav-item has-treeview menu-open">
                                    <Link  className="nav-link" to="posts">
                                        <i className="nav-icon fas fa-tachometer-alt" /><p>Posts</p>
                                    </Link>
                                </li> 
                                <li className="nav-item has-treeview menu-open">
                                    <Link  className="nav-link" to="persons">
                                        <i className="nav-icon fas fa-tachometer-alt" /><p>Persons</p>
                                    </Link>
                                </li> 
                                </ul>
                                </nav>
                                {/* /.sidebar-menu */}
                            </div>
                            {/* /.sidebar */}
                        </aside>
                    </div>
              <Switch>
                 <Route path="/dashboard">
                     <Dashboard />
                 </Route>
                 <Route path="/posts">
                     <AllPost />
                 </Route>
                 <Route path="/persons">
                     <AllPerson />
                 </Route>
              </Switch>
            </Router>
        
        )
    }
}
